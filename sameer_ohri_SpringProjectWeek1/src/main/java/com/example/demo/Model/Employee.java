package com.example.demo.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
 private long id;
 private String username;
 private String password;
 private String jobTitle;
 private double salary;

 public Employee() {
	super();
	// TODO Auto-generated constructor stub
}
 
public Employee(long id, String username, String password, String jobTitle, double salary) {
	super();
	this.id = id;
	this.username = username;
	this.password = password;
	this.jobTitle = jobTitle;
	this.salary = salary;
}

public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}

public String getUsername() {
	return username;
}

public void setUsername(String username) {
	this.username = username;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getJobTitle() {
	return jobTitle;
}

public void setJobTitle(String jobTitle) {
	this.jobTitle = jobTitle;
}

public double getSalary() {
	return salary;
}

public void setSalary(double salary) {
	this.salary = salary;
}

@Override
public String toString() {
	return "Employee [id=" + id + ", username=" + username + ", password=" + password + ", jobTitle=" + jobTitle
			+ ", salary=" + salary + "]";
}
 }
