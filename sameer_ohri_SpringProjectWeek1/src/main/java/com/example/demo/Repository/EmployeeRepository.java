package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.Model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	
	public Employee findEmployeeByUsername(String username);
	public Employee findEmployeeByUsernameAndPassword(String username, String password);


}
