package com.example.demo.Service;

import org.apache.catalina.authenticator.SavedRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Employee;
import com.example.demo.Repository.EmployeeRepository;

@Service
public class EmployeeService {

	EmployeeRepository empRepo;

	@Autowired
	public EmployeeService(EmployeeRepository empRepo) {
		super();
		this.empRepo = empRepo;
	}
	
	public Employee getEmployeeByUsername(Employee employee) {
		if(empRepo.findEmployeeByUsername(employee.getUsername()) == null)
		{
		   if(employee.getUsername()!=null || employee.getPassword()!=null || employee.getJobTitle()!=null || employee.getSalary()!=0)
		   
			   {
			   empRepo.save(employee);
		   return employee; 
		}}
		System.out.println("This user already exists...");
		return null;
	}
	
	public Employee validateLogin(Employee employee) {
		if(empRepo.findEmployeeByUsernameAndPassword(employee.getUsername(), employee.getPassword()) == null) {
			System.out.println("Invalid credentials...");
			return null;
		}
		return employee;
	}
}
