package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SameerOhriSpringProjectWeek1Application {

	public static void main(String[] args) {
		SpringApplication.run(SameerOhriSpringProjectWeek1Application.class, args);
	}

}
