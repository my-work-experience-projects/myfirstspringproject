package com.example.demo.Controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.Model.Employee;
import com.example.demo.Service.EmployeeService;

@Controller
public class EmployeeController {

	EmployeeService employeeService;

	@Autowired
	public EmployeeController(EmployeeService employeeService) {
		super();
		this.employeeService = employeeService;
	}
	
	@GetMapping("/Register")
	public String goToRegister(Model model) {
		model.addAttribute("employee", new Employee());
		return "Register";
	}
	
	@PostMapping("/processEmployee")
	public String processingEmployee(Employee employee, Model model) {
		if(employeeService.getEmployeeByUsername(employee) == null) {
			return "UserAlreadyExist";

		}
		model.addAttribute("username",employee.getUsername());
		return "confirmation";
	}
	
	@GetMapping("/Login")
	public String goToLogin(Employee employee, Model model) {
		model.addAttribute("employeeLogin",new Employee());
		return "Login";
	}
	
	@PostMapping("/checkCredentials")
	public String checkLogin(Employee employee, HttpSession session, Model model) {
		if(employeeService.validateLogin(employee) == null) {
            return "UserNotExist";

		}
		 session.setAttribute("username", employee.getUsername());
		return "UserHome";

	}
	
	@GetMapping("/UserHome")
	public String goToHome() {
		return "UserHome";
	}
}
