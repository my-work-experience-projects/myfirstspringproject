package com.example.demo;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.example.demo.Service.EmployeeService;
import com.example.demo.Model.Employee;


@WebMvcTest
class SameerOhriSpringProjectWeek1ApplicationTests {

	@Test
	void contextLoads() {
	}
	@Autowired

	MockMvc mvc;
	@MockBean
	EmployeeService empService;
	
	@Test
	void testRegisterCallsRegisterView() throws Exception
	{
		mvc.perform(MockMvcRequestBuilders.get("/Register"))
		.andExpect(MockMvcResultMatchers.view().name("Register"));
	}
	
	@Test
	void testRegisterToUserExistsAlready() throws Exception{
		when(empService.getEmployeeByUsername(Mockito.any())).thenReturn(new Employee());
		mvc.perform(MockMvcRequestBuilders.post("/processEmployee")
		.contentType(MediaType.APPLICATION_FORM_URLENCODED)	
				.param("username", "username")//setting up form parameters
				.param("password", "password")
		        .param("title", "title")
                .param("salary", "salary"))
		.andExpect(MockMvcResultMatchers.request().sessionAttribute("error", "user exists"))
		.andExpect(MockMvcResultMatchers.redirectedUrl("/UserAlreadyExist"));
	
	}



	}


