package com.example.demo;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.example.demo.Model.Employee;
import com.example.demo.Repository.EmployeeRepository;
import com.example.demo.Service.EmployeeService;

public class EmployeeServiceTest {
	
	private EmployeeService empService;
	EmployeeRepository mockEmpRepo = mock(EmployeeRepository.class);
	
	
	@BeforeEach
	public void setup() {
		empService = new EmployeeService(mockEmpRepo);
	}

	@Test
	void testGetEmployeeByUsernameMethod() {
		empService.getEmployeeByUsername(new Employee());
		when(mockEmpRepo.findEmployeeByUsername(anyString())).thenReturn(new Employee()); 

	}
	
	@Test
	void testLoginMethod() {
		empService.validateLogin(new Employee());
		when(mockEmpRepo.findEmployeeByUsernameAndPassword(anyString(), anyString())).thenReturn(new Employee()); 

		
	}
}
